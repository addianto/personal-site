FROM docker.io/python:3.9.7-alpine AS base

ARG PIPENV_VERSION=2021.5.29
ENV PIPENV_VENV_IN_PROJECT=True
ARG PELICAN_SITEURL
ENV PELICAN_SITEURL=${PELICAN_SITEURL:-http://localhost}

WORKDIR /src
COPY . .

RUN pip install pipenv==${PIPENV_VERSION} \
  && unset PIPENV_VERSION \
  && pipenv sync \
  && pipenv run pelican content \
    -o public \
    -s publishconf.py

FROM docker.io/nginx:1.21.3-alpine

COPY --from=base /src/public /usr/share/nginx/html
