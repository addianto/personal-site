# Daya's Notes

[![pipeline status](https://gitlab.com/addianto/personal-site/badges/master/pipeline.svg)](https://gitlab.com/addianto/personal-site/-/commits/master)

## Requirements

- [Python](https://www.python.org/) >= 3.6
- [`pipenv`](https://pipenv.kennethreitz.org/en/latest/)

## Getting Started

Create a new virtual environment and download all of the required packages using
`pipenv`:

```shell
pipenv sync
```

Activate the virtual environment and generate (or, publish) the static pages:

```shell
pipenv shell
pelican content
```

The publish process read the following environment variables from the shell:

- `PELICAN_SITEURL` Base URL of the web site. Defaults to empty (localhost).
- `PELICAN_TIMEZONE` The timezone used in the date information. Defaults to
  `Asia/Jakarta`.

To serve the static pages locally:

```shell
pelican --listen
```

## License

Creative Commons Attribution 4.0 International.
