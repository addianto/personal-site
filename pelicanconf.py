from datetime import datetime
import os

AUTHOR = 'Daya Adianto'
SITENAME = "Daya's Notes"
SITETITLE = "Daya Adianto"
SITEURL = os.getenv('PELICAN_SITEURL', '')

PATH = 'content'

TIMEZONE = os.getenv('PELICAN_TIMEZONE', 'Asia/Jakarta')

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = ()

# Social widget
SOCIAL = (
    ('Twitter', 'https://twitter.com/addianto'),
    ('GitHub', 'https://github.com/addianto'),
    ('GitLab', 'https://gitlab.com/addianto'),
)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

BASE_PATH = os.getcwd()
TYPOGRIFY = True
THEME = f'{BASE_PATH}{os.sep}themes{os.sep}flex{os.sep}'

CC_LICENSE = {
    'name': 'Creative Commons Attribution 4.0 International License',
    'version': '4.0',
    'slug': 'by',
    'icon': True,
    'language': 'en_US',
}

COPYRIGHT_NAME = 'Daya Adianto'
COPYRIGHT_YEAR = datetime.now().year

USE_LESS = True
