Title: Contact
Date: 2021-11-02 15:31
Modified: 2021-11-02 16:00
Slug: contact

I am currently employed at Universitas Indonesia, specifically at
[Pusilkom UI](https://pusilkom.ui.ac.id) and the
[Faculty of Computer Science](https://cs.ui.ac.id). You can email me at
**`myfullnameinoneword`** _@_ **`cs.ui.ac.id`** or
**`myfullnameinoneword`** _@_ **`ui.ac.id`**.
For obvious reason, i.e. avoiding spam, do not forget to replace
**`myfullnameinoneword`** with my **actual full name** written in one word. ;)

To schedule a work- or academic-related (e.g. office hours) appointment, please
check my [online calendar](https://dayaadianto-ui.youcanbook.me/) below.

<iframe src="https://dayaadianto-ui.youcanbook.me/?noframe=true&skipHeaderFooter=true" id="ycbmiframedayaadianto-ui" style="width:100%;height:1000px;border:0px;background-color:transparent;" frameborder="0" allowtransparency="true"></iframe><script>window.addEventListener && window.addEventListener("message", function(event){if (event.origin === "https://dayaadianto-ui.youcanbook.me"){document.getElementById("ycbmiframedayaadianto-ui").style.height = event.data + "px";}}, false);</script>

I do have my own personal email address, but I am not going to put it publicly.
At least for now. If there are personal matters you wish to discuss, please
contact me through my work email. I will reply your email through my personal
email address.
